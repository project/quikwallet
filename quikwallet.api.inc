<?php

/**
 * @file
 * cURL call which will initiate the curl call on payment getway.
 */

/**
 * Preparing cURL Data.
 */
class CurlCall {

  /**
   * Description.
   *
   * @var ch
   *   curlresource
   */
  protected $ch = NULL;

  /**
   * Description.
   *
   * @var options
   *   to set for curl call.
   */
  protected $options = NULL;

  /**
   * Description.
   *
   * @var response
   * returned from server.
   */
  protected $response = NULL;

  /**
   * We will initiate curl here. The constructor.
   */
  public function __construct() {
    $this->ch = curl_init();
  }

  /**
   * The destructor.
   */
  public function __destruct() {
    if (is_resource($this->ch)) {
      curl_close($this->ch);
    }
    $this->ch = NULL;
  }

  /**
   * Sets default options for the curl call.
   */
  public function setDefaultOptions() {
    $this->options = array(
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_HEADER         => FALSE,
      CURLOPT_ENCODING       => 'gzip,deflate',
      CURLOPT_CONNECTTIMEOUT => 15,
      CURLOPT_TIMEOUT        => 30,
      CURLOPT_SSL_VERIFYPEER => FALSE,
      /* These may be required depending on the cURL configuration
        CURLOPT_SSL_VERIFYHOST => 1,
        CURLOPT_CAINFO => "../certs/cacert.pem",
        CURLOPT_HTTPHEADER => array('Host: uat.quikpay.in'),
      */
    );
  }

  /**
   * Post call.
   *
   * @param string $url
   *   Takes the $url to call to.
   * @param string $reqParams
   *   Extra $reqParams params data to send with request.
   */
  public function post($url, $reqParams) {
    $this->setDefaultOptions();
    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt_array($this->ch, $this->options);
    curl_setopt($this->ch, CURLOPT_POST, 1);
    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $reqParams);

    return $this->request();
  }

  /**
   * Executes the curl request.
   */
  public function request() {
    $this->response = curl_exec($this->ch);
    if ($this->response === FALSE) {
      $this->response = curl_error($this->ch);
    }

    return $this->response;
  }

}
